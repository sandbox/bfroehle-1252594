<?php

/**
 * @file
 *   Theme hooks for the budgetfield field widget and formatters.
 */

/**
 * Returns total amount in the specified column.
 *
 * @param $variables
 *   An associative array with the following keys:
 *   - items: A list of budget line items.
 *   - column: The desired column to be totaled.
 *   - number_prefix: A prefix, i.e., '$'
 */
function theme_budgetfield_formatter_total($variables) {
  extract($variables);

  $total = 0;
  foreach ($items as $delta => $item) {
    $total += $item[$column];
  }

  return $number_prefix . number_format($total, 2);
}

/**
 * Returns HTML for a budgetfield, displayed as a table.
 *
 * The budgetfield is displayed with the following columns:
 * - label: Description
 * - quantity: Quantity
 * - unitcost: Unit Cost
 * - requested: Requested Amount
 * - remainder: Remaining Amount
 * - total: Total Amount
 *
 * Of these, the first four columns are provided. The other two columns are
 * calculated as:
 *  total = quantity * unitcost
 *  remainder = total - requested
 *
 * @param $variables
 *   An associative array containing:
 *   - header: An associative array containing the human-readable column names,
 *     keyed by the machine-readable names: label, quantity, unitcost,
 *     requested, remainder, total.
 *   - items: An array of line items, each an associative array containing:
 *     - label: Description.
 *     - quantity: Quantity.
 *     - unitcost: Cost per unit quantity.
 *     - request: Amount requested.
 */
function theme_budgetfield_formatter_table($variables) {
  $header = array();
  foreach (_budgetfield_columns() as $column) {
    $header[] = $variables['header'][$column];
  }

  // Build a list of the total amount requested.
  $totals = array(
    'request' => 0,
    'remainder' => 0,
    'total' => 0,
  );

  foreach ($variables['items'] as $delta => $item) {
    // Add row.
    $rows[] = array(
      $item['label'],
      // The quantity is stored internally as a number with two decimal digits.
      // However on display we'll show it as an integer if it is a whole number.
      number_format($item['quantity'], ((int) $item['quantity'] == $item['quantity']) ? 0 : 2),
      '$' . number_format($item['unitcost'], 2),
      '$' . number_format($item['request'], 2),
      '$' . number_format($item['remainder'], 2),
      '$' . number_format($item['total'], 2),
    );

    // Keep running total.
    $totals['request'] += $item['request'];
    $totals['remainder'] += $item['remainder'];
    $totals['total'] += $item['total'];
  }

  // Print a blank row.
  $rows[] = array(
    'data' => array(
      array(
        'data' => '',
        'colspan' => 6,
      ),
    ),
  );
  // Display a summary row with the total amounts.
  $rows[] = array(
    'data' => array(
      array(
        'data' => t('Total'),
        'colspan' => 3,
      ),
      '$' . number_format($totals['request'], 2),
      '$' . number_format($totals['remainder'], 2),
      '$' . number_format($totals['total'], 2),
    ),
    'class' => array('grand_total'),
  );

  return theme('table', array('header' => $header, 'rows' => $rows));
}

/**
 * Returns HTML for a budgetfield entry widget, displayed as a table.
 *
 * @param $variables
 *   An associative array containing:
 *   - element: An associative array containg the properties of the element,
 *     as produced by budgetfield_field_formatter_view(). Properties include:
 *     - #header: Column header names.
 *     - 0, 1, 2, .. : Line items
 */
function theme_budgetfield_widget_table($variables) {
  $element = $variables['element'];
  $weight_class = $element['#id'] . '-weight';
  $table_id = $element['#id'] . '-table';

  $header = array();
  foreach (_budgetfield_columns() as $column) {
    $header[] = $element['#header'][$column];
  }
  $header[] = t('Weight');

  // Get our list of widgets in order (needed when the form comes back after
  // preview or failed validation).
  $items = array();
  foreach (element_children($element) as $key) {
    if ($key === 'add_more') {
      // This element holds the "Update totals" and "Add new item" buttons
      // that appear below the table.
      $actions = &$element[$key];
    }
    elseif ($key === 'totals') {
      $totals = &$element[$key];
    }
    else {
      $items[] = &$element[$key];
    }
  }
  usort($items, '_field_sort_items_value_helper');

  // Prepare a list of rows, one for each budget line item.
  $rows  = array();
  foreach ($items as &$item) {

    // Build each cell in the row, beginning with the budgetfield columns.
    $row = array();
    foreach (_budgetfield_columns() as $key) {
      $row[] = array(
        'data' => drupal_render($item[$key]),
        'class' => array('budgetfield-' . $key),
      );
    }

    // Include the weight column for use with tabledrag.
    $item['_weight']['#attributes']['class'] = array($weight_class);
    $row[] = drupal_render($item['_weight']);

    $rows[] = array(
      'data' => $row,
      'class' => array('draggable'),
    );
    drupal_render($item);
  }

  // Add a footer row, using the totals provided.
  $rows[] = array(
    'data' => array(
      array(
        'data' => t('Total'),
        'colspan' => 3,
      ),
      drupal_render($totals['request']),
      drupal_render($totals['remainder']),
      drupal_render($totals['total']),
      '',
    ),
    'class' => array('grand_total'),
  );

  $output = '<div class="form-item">';
  $output .= theme('table', array('header' => $header, 'rows' => $rows, 'attributes' => array('id' => $table_id)));
  $output .= $element['#budgetfield_description'] ? '<div class="description">' . $element['#budgetfield_description'] . '</div>' : '';
  $output .= '<div class="clearfix">' . drupal_render($actions) . '</div>';
  $output .= '</div>';

  drupal_add_tabledrag($table_id, 'order', 'sibling', $weight_class);

  return $output;
}

