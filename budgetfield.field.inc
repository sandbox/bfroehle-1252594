<?php

/**
 * @file
 * Provides a field type for the budgetfield element.
 *
 * Includes two widgets for data collection:
 * - standard: A basic form.
 * - table: A formatted table.
 *
 * Includes two formatters for data display:
 * - table: A formatted table view.
 * - total: A calculation for the total of a specific column.
 */

/**
 * Implements hook_field_info().
 */
function budgetfield_field_info() {
  $fields = array();

  $fields['budgetfield'] = array(
    'label' => t('Budget'),
    'description' => t('A budget request form.'),
    'settings' => array(),
    'instance_settings' => array(
      'header' => _budgetfield_default_header(),
    ),
    'default_widget' => 'budgetfield_table',
    'default_formatter' => 'budgetfield_table',
  );

  return $fields;
}

/**
 * Implements hook_field_load().
 *
 * Compute the total and remainder fields.
 */
function budgetfield_field_load($entity_type, $entities, $field, $instances, $langcode, &$items, $age) {
  foreach ($entities as $id => $entity) {
    foreach ($items[$id] as $delta => &$item) {
      $item['total'] = number_format($item['quantity']*$item['unitcost'], 2, '.', '');
      $item['remainder'] = number_format($item['total']-$item['request'], 2, '.', '');
    }
  }
}

/**
 * Implements hook_field_is_empty().
 */
function budgetfield_field_is_empty($item) {
  return $item['label'] == '' && $item['quantity'] == '' && $item['unitcost'] == '' && $item['request'] == '';
}

/**
 * Helper function for budgetfield_field_validate().
 *
 * @param $item
 *   A budgetfield item.
 *
 * @return
 *   TRUE if the budget line item is completely empty, FALSE otherwise.
 */
function budgetfield_field_is_nonempty($item) {
  return !budgetfield_field_is_empty($item);
}

/**
 * Implements hook_field_validate().
 */
function budgetfield_field_validate($entity_type, $entity, $field, $instance, $langcode, $items, &$errors) {
  unset($items['add_more']);

  // Filter out empty rows.
  $items = array_filter($items, 'budgetfield_field_is_nonempty');

  // Verify that at least one row is included if the field is required.
  if (count($items) == 0 && $instance['required']) {
    $errors[$field['field_name']][$langcode][0][] = array(
      'error' => 'budgetfield_error',
      'message' => t('@label field is required.', array('@label' => $instance['label'])),
    );
  }

  // Individual items are already validated by the budgetfield element.

  // foreach ($items as $delta => $item) {
  //   // @todo: Create a running total, use to validate the total request?
  // }
}

/**
 * Implements hook_field_widget_error().
 */
function budgetfield_field_widget_error($element, $error, $form, &$form_state) {
  form_error($element, $error['message']);
}

/**
 * Implements hook_field_instance_settings_form().
 */
function budgetfield_field_instance_settings_form($field, $instance) {
  $settings = $instance['settings'];

  $form['header'] = array(
    '#type' => 'fieldset',
    '#title' => 'Headers',
    '#description' => 'Labels used when displaying the budgets.',
  );

  foreach (_budgetfield_columns() as $key) {
    $form['header'][$key] = array(
      '#type' => 'textfield',
      '#title' => t('%key', array('%key' => $key)),
      '#size' => 30,
      '#default_value' => $settings['header'][$key],
    );
  }

  return $form;
}

/**
 * Widgets (data collection).
 */

/**
 * Implements hook_field_widget_info().
 */
function budgetfield_field_widget_info() {
  $widgets = array();

  $widgets['budgetfield_standard'] = array(
    'label' => t('Fieldset'),
    'field types' => array('budgetfield'),
    'settings' => array(),
    'behaviors' => array(
      'multiple values' => FIELD_BEHAVIOR_DEFAULT,
      'default value' => FIELD_BEHAVIOR_NONE,
    ),
  );

  $widgets['budgetfield_table'] = array(
    'label' => t('Table with column sums'),
    'field types' => array('budgetfield'),
    'settings' => array(),
    'behaviors' => array(
      'multiple values' => FIELD_BEHAVIOR_CUSTOM,
      'default value' => FIELD_BEHAVIOR_NONE,
    ),
  );

  return $widgets;
}

/**
 * Implements hook_field_widget_form().
 */
function budgetfield_field_widget_form(&$form, &$form_state, $field, $instance, $langcode, $items, $delta, $element) {
  if ($instance['widget']['type'] == 'budgetfield_standard') {
    $element += array(
      '#type' => 'budgetfield',
      '#default_value' => isset($items[$delta]) ? $items[$delta] : NULL,
      '#header' => $instance['settings']['header'],
    );
    return $element;
  }
  elseif ($instance['widget']['type'] == 'budgetfield_table') {
    $field_name = $field['field_name'];
    $parents = $form['#parents'];
    $field_state = field_form_get_state($parents, $field_name, $langcode, $form_state);

    $defaults = _budgetfield_empty_item();

    // Update order according to weight.
    $items = _field_sort_items($field, $items);
    $count = count($items);

    // Determine the number of widgets to display.
    switch ($field['cardinality']) {
      case FIELD_CARDINALITY_UNLIMITED:
        $max = $field_state['items_count']+1;
        break;

      default:
        $max = $field['cardinality'];
        break;
    }

    for ($i = $count; $i < $max; $i++) {
      $items[] = $defaults;
    }

    $id_prefix = implode('-', array_merge($parents, array($field_name)));
    $wrapper_id = drupal_html_id($id_prefix . '-add-more-wrapper');

    $element += array(
      '#type' => 'item',
      '#theme' => 'budgetfield_widget_table',
      '#header' => $instance['settings']['header'],
      '#process' => array('budgetfield_table_widget_process_multiple'),
      '#pre_render' => array('budgetfield_table_widget_pre_render_multiple'), // We compute the grand totals here.
      '#prefix' => '<div id="' . $wrapper_id . '">',
      '#suffix' => '</div>',
    );

    $element['#budgetfield_description'] = $element['#description'];
    unset($element['#description']);

    foreach ($items as $delta => $item) {
      $element[$delta] = array(
        '#type' => 'budgetfield',
        '#default_value' => $items[$delta],
        '#weight' => $delta,
        '#header' => $instance['settings']['header'],
        '#title_display' => 'invisible',
      );
    }

    // Add a grand total line item.
    foreach (array('request', 'remainder', 'total') as $key) {
      $element['totals'][$key] = array(
        '#type' => 'item',
        '#title' => check_plain($element['#header'][$key]),
        '#title_display' => 'invisible',
        '#field_prefix' => '$',
        // Since we do not have easy access to the $element['#value'] here, we
        // delay computing the grand totals until the pre-render phase.
        // @see budgetfield_table_widget_pre_render_multiple().
        '#markup' => '',
      );
    }

    // This element should really be called 'actions', but the field
    // functionality in core automatically removes the 'add_more' field
    // from the values on submit. This could probably be worked around
    // by setting #value_callback, but it works fine as is.
    $element['add_more'] = array(
      '#type' => 'actions',
    );
    $element['add_more']['update_totals'] = array(
      '#type' => 'submit',
      '#name' => strtr($id_prefix, '-', '_') . '_update_totals',
      '#value' => t('Update totals'),
      '#limit_validation_errors' => array(array_merge($parents, array($field_name, $langcode))),
      '#submit' => array('budgetfield_update_totals_submit'),
      '#ajax' => array(
        'callback' => 'budgetfield_refresh_js',
        'wrapper' => $wrapper_id,
        'effect' => 'fade',
      ),
    );

    // Add 'add more' button, if not working with a programmed form.
    if ($field['cardinality'] == FIELD_CARDINALITY_UNLIMITED && empty($form_state['programmed'])) {
      $element['add_more']['add_more'] = array(
        '#type' => 'submit',
        '#name' => strtr($id_prefix, '-', '_') . '_add_more_add_more',
        '#value' => t('Add another item'),
        '#attributes' => array('class' => array('field-add-more-submit')),
        '#limit_validation_errors' => array(array_merge($parents, array($field_name, $langcode))),
        '#submit' => array('budgetfield_add_more_submit'),
        '#ajax' => array(
          'callback' => 'budgetfield_refresh_js',
          'wrapper' => $wrapper_id,
          'effect' => 'fade',
        ),
      );
    }

    return $element;
  }
}

/**
 * Submit handler for the "Update totals" button of a field form.
 *
 * @see field_add_more_submit()
 */
function budgetfield_update_totals_submit($form, &$form_state) {
  // Just rebuild the form to update the totals.
  $form_state['rebuild'] = TRUE;
}

/**
 * Submit handler for the "Add more" button of a field form.
 *
 * @see field_add_more_submit()
 */
function budgetfield_add_more_submit($form, &$form_state) {
  $button = $form_state['triggering_element'];

  // Go two levels up in the form, to the budgetfield widget container.
  $element = drupal_array_get_nested_value($form, array_slice($button['#array_parents'], 0, -2));
  $field_name = $element['#field_name'];
  $langcode = $element['#language'];
  $parents = $element['#field_parents'];

  // Increment the items count.
  $field_state = field_form_get_state($parents, $field_name, $langcode, $form_state);
  $field_state['items_count']++;
  field_form_set_state($parents, $field_name, $langcode, $form_state, $field_state);

  $form_state['rebuild'] = TRUE;
}

/**
 * Ajax callback in response to a new empty widget being added to the form.
 *
 * This returns the new page content to replace the page content made obsolete
 * by the form submission.
 *
 * @see field_add_more_js()
 */
function budgetfield_refresh_js($form, $form_state) {
  $button = $form_state['triggering_element'];

  // Go two levels up in the form, to the budgetfield widget container.
  $element = drupal_array_get_nested_value($form, array_slice($button['#array_parents'], 0, -2));

  return $element;
}

/**
 * An element #process callback for a set of budgetfields.
 *
 * Adds the weight field to each row so it can be ordered.
 */
function budgetfield_table_widget_process_multiple($element, &$form_state, $form) {
  $element_children = element_children($element, TRUE);
  $count = count($element_children);

  foreach ($element_children as $delta => $key) {
    if ($key === 'add_more' || $key === 'totals') {
      continue;
    }

    $description = $element[$key]['#default_value']['label'];
    $element[$key]['_weight'] = array(
      '#type' => 'weight',
      '#title' => $description ? t('Weight for @title', array('@title' => $description)) : t('Weight for new line item'),
      '#title_display' => 'invisible',
      '#delta' => $count,
      '#default_value' => $delta,
    );
  }

  return $element;
}

/**
 * An element #pre_render callback for a set of budgetfields.
 *
 * Calculates the grand total of request, remainder, and total columns, and
 * sets the values of those form elements.
 */
function budgetfield_table_widget_pre_render_multiple($element) {
  $columns = array('request', 'remainder', 'total');

  foreach ($columns as $column) {
    $totals[$column] = 0;
  }

  // Compute the sum of each column.
  foreach (element_children($element) as $key) {
    if (isset($element[$key]['#type']) && $element[$key]['#type'] === 'budgetfield') {
      foreach ($columns as $column) {
        $totals[$column] += $element[$key]['#value'][$column];
      }
    }
  }

  foreach ($columns as $column) {
    $child = &$element['totals'][$column];

    // Create a unique ID for the span that will hold the grand total.
    $child_id = $child['#id'] . '-jswrapper';
    $settings[$column] = '#' . $child_id;

    // Set the grand total for those without Javascript.
    $child['#markup'] = '<span id="' . $child_id . '">' . number_format($totals[$column], 2) . '</span>';
  }

  // We don't need to attach the budgetfield.js file since each row is already
  // attaching the file.
  $element['#attached']['js'][] = array(
    'type' => 'setting',
    'data' => array(
      'budgetFieldTotal' => array(
        '#' . $element["#id"] => $settings,
      ),
    ),
  );
  return $element;
}

/**
 * Formatters (display).
 */

/**
 * Implements hook_field_formatter_info().
 */
function budgetfield_field_formatter_info() {
  $formatters = array();

  $formatters['budgetfield_table'] = array(
    'label' => t('Table'),
    'field types' => array('budgetfield'),
    'settings' => array(
      'total' => TRUE,
    ),
  );

  $formatters['budgetfield_total'] = array(
    'label' => t('Total (of one column)'),
    'field types' => array('budgetfield'),
    'settings' => array(
      'column' => 'request',
    ),
  );

  return $formatters;
}

/**
 * Implements hook_field_formatter_settings_form().
 */
function budgetfield_field_formatter_settings_form($field, $instance, $view_mode, $form, &$form_state) {
  $display = $instance['display'][$view_mode];
  $settings = $display['settings'];
  $formatter = $display['type'];

  if ($formatter == 'budgetfield_total') {
    $form = array();
    $form['column'] = array(
      '#title' => t('Column'),
      '#type' => 'select',
      '#options' => array(
        'request' => $instance['settings']['header']['request'],
        'remainder' => $instance['settings']['header']['remainder'],
        'total' => $instance['settings']['header']['total'],
      ),
      '#default_value' => $settings['column'],
      '#description' => t('The column whose total should be computed.'),
    );
    return $form;
  }
}

/**
 * Implements hook_field_formatter_settings_summary().
 */
function budgetfield_field_formatter_settings_summary($field, $instance, $view_mode) {
  $display = $instance['display'][$view_mode];
  $settings = $display['settings'];
  $formatter = $display['type'];
  $header = $instance['settings']['header'];

  if ($formatter == 'budgetfield_total') {
    return t('Display the total of the %column column.', array('%column' => $header[$settings['column']]));
  }
}

/**
 * Implements hook_field_formatter_view().
 */
function budgetfield_field_formatter_view($entity_type, $entity, $field, $instance, $langcode, $items, $display) {
  $formatter = $display['type'];
  $element = array();

  if ($formatter == 'budgetfield_table') {
    $element[0] = array(
      '#theme' => 'budgetfield_formatter_table',
      '#items' => $items,
      '#header' => $instance['settings']['header'],
    );
  }
  elseif ($formatter == 'budgetfield_total') {
    $element[0] = array(
      '#theme' => 'budgetfield_formatter_total',
      '#items' => $items,
      '#column' => $display['settings']['column'],
      '#number_prefix' => '$',
    );
  }
  return $element;
}
