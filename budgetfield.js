(function ($) {
  
/**
 * Attach the budgetfield form element behavior.
 */
Drupal.behaviors.budgetFieldItem = {
  /**
   * Attaches the behavior.
   *
   * @param settings.budgetFieldItem
   *   A list of elements to process, keyed by HTML ID of the element
   *   containing the total value. Each element is an object defining
   *   the following properties:
   *   - quantity: The HTML ID of the quantity form element.
   *   - unitcost: The HTML ID of the unitcost form element.
   *   - request: The HTML ID of the request form element.
   *   - remainder: The HTML ID of the container into which the calculated
   *       remainder value should be inserted.
   *   - total: The HTML ID of the container into which the calculated total
   *       value should be inserted.
   */
  attach: function (context, settings) {
    var self = this;
    $.each(settings.budgetFieldItem, function(total_id, options) {
      var $quantity = $(options.quantity, context);
      var $unitcost = $(options.unitcost, context);
      var $request = $(options.request, context);
      var $remainder = $(options.remainder, context);
      var $total = $(options.total, context);

      // All elements have to exist.
      if (!$quantity.length || !$unitcost.length || !$request.length || !$remainder.length || !$total.length) {
        return;
      }

      if ($request.val().length == 0) {
        $request.addClass('budgetfield-autocompute');
      }

      // Recompute the line item total.
      var recomputeTotals = function (event) {
        if (event && event.data && event.data.is_request) {
          $request.removeClass('budgetfield-autocompute')
        }

        var total = parseFloat($quantity.val().replace(',','') * $unitcost.val().replace(',','')).toFixed(2);
        $total.text(number_format(total, 2));

        if ($request.hasClass('budgetfield-autocompute') && $quantity.val().length && $unitcost.val().length) {
          $request.val(total);
        }

        var remainder = parseFloat($total.text() - $request.val()).toFixed(2);
        $remainder.text(number_format(remainder, 2));
      }

      $quantity.bind('keyup.budgetFieldItem change.budgetFieldItem', recomputeTotals)
      $unitcost.bind('keyup.budgetFieldItem change.budgetFieldItem', recomputeTotals)
      $request.bind('keyup.budgetFieldItem change.budgetFieldItem', {is_request: true}, recomputeTotals)
    })
  },
}

Drupal.behaviors.budgetFieldTotal = {
  /**
   * Attaches the behavior.
   *
   * @param settings.budgetFieldTotal
   *   A list of elements to process, keyed by HTML ID of the element
   *   containing the entire multiline budgetfield. Each element is an object
   *   defining the following properties:
   *   - request: The HTML ID of the container into which the calculated grand
    *      total requested value should be inserted.
   *   - remainder: The HTML ID of the container into which the calculated
   *       grand total remainder value should be inserted.
   *   - total: The HTML ID of the container into which the calculated grand
   *       total value should be inserted.
   *   Each total is computed by summing the values of all elements matching
   *   #BUDGETFIELD_ID .budgetfield-KEY-value. These classes are assumed to
   *   be set in the HTML already, and any line item totals are assumed to
   *   already be calculated.
   */
  attach: function(context, settings) {
    var self = this;
    $.each(settings.budgetFieldTotal, function(budgetfield_id, options) {
      var $grandremainder = $(options.remainder, context)
      var $grandrequest = $(options.request, context)
      var $grandtotal = $(options.total, context)

      var $quantities = $(budgetfield_id.concat(' .budgetfield-quantity-value'));
      var $unitcosts = $(budgetfield_id.concat(' .budgetfield-unitcost-value'));
      var $requests = $(budgetfield_id.concat(' .budgetfield-request-value'));
      var $remainders = $(budgetfield_id.concat(' .budgetfield-remainder-value'));
      var $totals = $(budgetfield_id.concat(' .budgetfield-total-value'));

      // Recompute the grand totals.
      var recomputeGrandTotals = function () {
        var request = 0.0;
        $requests.each(function(idx) {
          var val = $(this).val().replace(',','');
          if (val.length > 0) {
            request += parseFloat(val);
          }
        });

        var remainder = 0.0;
        $remainders.each(function(idx) {
          var val = $(this).text().replace(',','');
          if (val.length > 0) {
            remainder += parseFloat(val);
          }
        });

        var total = 0.0;
        $totals.each(function(idx) {
          var val = $(this).text().replace(',','');
          if (val.length > 0) {
            total += parseFloat(val);
          }
        });
        $grandrequest.text(number_format(request, 2));
        $grandremainder.text(number_format(remainder, 2));
        $grandtotal.text(number_format(total, 2));
      }

      $quantities.bind('keyup.budgetFieldItem change.budgetFieldItem', recomputeGrandTotals)
      $unitcosts.bind('keyup.budgetFieldItem change.budgetFieldItem', recomputeGrandTotals)
      $requests.bind('keyup.budgetFieldItem change.budgetFieldItem', recomputeGrandTotals)
    })
  }
}

/**
 * Formats a number with grouped thousands.
 *
 * The code for number_format is from php.js and licensed under MIT/GPL.
 *
 * @see http://phpjs.org/functions/number_format:481
 * @see http://phpjs.org/pages/license
 */
function number_format(number, decimals, dec_point, thousands_sep) {
    number = (number + '').replace(/[^0-9+\-Ee.]/g, '');
    var n = !isFinite(+number) ? 0 : +number,
        prec = !isFinite(+decimals) ? 0 : Math.abs(decimals),
        sep = (typeof thousands_sep === 'undefined') ? ',' : thousands_sep,
        dec = (typeof dec_point === 'undefined') ? '.' : dec_point,
        s = '',
        toFixedFix = function(n, prec) {
            var k = Math.pow(10, prec);
            return '' + Math.round(n * k) / k;
        };
    // Fix for IE parseFloat(0.55).toFixed(0) = 0;
    s = (prec ? toFixedFix(n, prec) : '' + Math.round(n)).split('.');
    if (s[0].length > 3) {
        s[0] = s[0].replace(/\B(?=(?:\d{3})+(?!\d))/g, sep);
    }
    if ((s[1] || '').length < prec) {
        s[1] = s[1] || '';
        s[1] += new Array(prec - s[1].length + 1).join('0');
    }
    return s.join(dec);
}

})(jQuery);

