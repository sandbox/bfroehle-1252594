<?php

/**
 * @file
 * Defines a budget element which contains a label, quantity, unit cost, and
 * requested amount. The total cost and other funding amount are calculated
 * automatically.
 */

// Load all Field module hooks for budgetfield.
require_once 'budgetfield.field.inc';

/**
 * Implements hook_theme().
 */
function budgetfield_theme() {
  return array(
    'budgetfield_formatter_table' => array(
      'variables' => array('items' => NULL, 'header' => NULL),
      'file' => 'budgetfield.theme.inc',
    ),
    'budgetfield_formatter_total' => array(
      'variables' => array('items' => NULL, 'column' => NULL, 'number_prefix' => ''),
      'file' => 'budgetfield.theme.inc',
    ),
    'budgetfield_widget_table' => array(
      'render element' => 'element',
      'file' => 'budgetfield.theme.inc',
    ),
  );
}

/**
 * Implements hook_element_info().
 *
 * The budgetfield element may be used independetly anywhere in Drupal.
 */
function budgetfield_element_info() {
  $types['budgetfield'] = array(
    '#input' => TRUE,
    '#process' => array('budgetfield_budgetfield_process'),
    '#pre_render' => array('budgetfield_budgetfield_pre_render'),
    '#value_callback' => 'budgetfield_budgetfield_value',
    '#element_validate' => array('budgetfield_budgetfield_validate'),
  );
  return $types;
}

/**
 * Process function to expand the budgetfield element type.
 */
function budgetfield_budgetfield_process($element, &$form_state, $form) {
  $element['#tree'] = TRUE;

  // Get a list of field labels from the provided #header, or use defaults.
  $element += array('#header' => array());
  $element['#header'] += _budgetfield_default_header();

  $element_defaults = array();
  if (isset($element['#title_display'])) {
    $element_defaults['#title_display'] = $element['#title_display'];
  }

  // Create the form elements in the budgetfield.
  $element['label'] = array(
    '#type' => 'textfield',
    '#title' => check_plain($element['#header']['label']),
    '#default_value' => $element['#default_value']['label'],
    '#maxlength' => 255,
  ) + $element_defaults;
  foreach (array('quantity', 'unitcost', 'request') as $key) {
    $val = $element['#default_value'][$key];
    if ($val != '' && $key == 'quantity' && $val == (int) $val) {
      $val = (int) $val;
    }
    $element[$key] = array(
      '#type' => 'textfield',
      '#title' => $element['#header'][$key],
      '#field_prefix' => ($key !== 'quantity') ? '$' : NULL,
      '#default_value' => $val,
      '#size' => 14, // precision + 4
      '#maxlength' => 12, // precision + 2
      '#attributes' => array(
        'class' => array('budgetfield-' . $key . '-value'),
      ),
    ) + $element_defaults;
  }

  foreach (array('remainder', 'total') as $key) {
    $id = $element['#id'] . '-' . $key . '-jswrapper';
    $element[$key] = array(
      '#type' => 'item',
      '#title' => $element['#header'][$key],
      '#field_prefix' => '$',
      '#markup' => '<span id="' . $id . '" class="budgetfield-' . $key . '-value">' . number_format($element['#value'][$key], 2) . '</span>',
    ) + $element_defaults;
  }

  return $element;
}

/**
 * Pre-render callback for budgetfield element.
 *
 * Adds the autocompute JS to the line item.
 */
function budgetfield_budgetfield_pre_render($element) {
  // Build a list of element id's for the form $element;
  $id = '#' . $element["#id"];
  $settings = array(
    'quantity' => '#' . $element['quantity']['#id'],
    'unitcost' => '#' . $element['unitcost']['#id'],
    'request' => '#' . $element['request']['#id'],
    'total' => $id . '-total-jswrapper',
    'remainder' => $id . '-remainder-jswrapper',
  );

  $element['#attached']['js'][] = drupal_get_path('module', 'budgetfield') . '/budgetfield.js';
  $element['#attached']['js'][] = array(
    'type' => 'setting',
    'data' => array(
      'budgetFieldItem' => array(
        $id => $settings,
      ),
    ),
  );
  return $element;
}

/**
 * Value callback for budgetfield element.
 *
 * @see budgetfield_field_load().
 */
function budgetfield_budgetfield_value(&$element, $input = FALSE, $form_state = NULL) {
  if ($input === FALSE) {
    // This is called to convert $element['#default_value'] into
    // $element['#value']. We want to do the same conversion, so just set
    // $input to what we need to process.
    $input = $element['#default_value'];
  }
  $input['total'] = number_format($input['quantity']*$input['unitcost'], 2, '.', '');
  $input['remainder'] = number_format($input['total']-$input['request'], 2, '.', '');
  return $input;
}

/**
 * Test if the budgetfield is empty.
 *
 * @param
 *   The budgetfield value, as from $element['#value'].
 */
function _budgetfield_budgetfield_is_empty($item) {
  return $item['label'] == '' && $item['quantity'] == '' && $item['unitcost'] == '' && $item['request'] == '';
}

/**
 * Validate the budgetfield element.
 */
function budgetfield_budgetfield_validate(&$element, &$form_state) {
  // Check if the budgetfield element has content.
  if (_budgetfield_budgetfield_is_empty($element['#value'])) {
    if (!empty($element['#required'])) {
      // Set an error if the element is required but empty.
      form_error($element, t('@title is required.', array('@title' => $element['#title'])));
    }
    // There is no need to further validate an empty budgetfield.
    return;
  }

  // Validate the label.
  if ($element['#value']['label'] == '') {
    form_error($element['label'], t('@title is required.', array('@title' => $element['#header']['label'])));
  }

  // Validate the numeric columns.
  $all_numeric = TRUE;

  foreach (array('quantity', 'unitcost', 'request') as $column) {
    $value = $element['#value'][$column];
    if ($value == '') {
      form_error($element[$column], t('@title is required.', array('@title' => $element['#header'][$column])));
      $all_numeric = FALSE;
    }
    elseif (!is_numeric($value)) {
      form_error($element[$column], t('@title must be a number.', array('@title' => $element['#header'][$column])));
      $all_numeric = FALSE;
    }
    elseif ($value < 0) {
      form_error($element[$column], t('@title must be non-negative.', array('@title' => $element['#header'][$column])));
      $all_numeric = FALSE;
    }
    elseif (round($value, 2) != $value) {
      form_error($element[$column], t('Only two decimal places are allowed in the @title field.', array('@title' => $element['#header'][$column])));
      $all_numeric = FALSE;
    }
  }

  // We cannot proceed with validation unless all of the numeric columns
  // actually contain valid numbers.
  if (!$all_numeric) {
    return;
  }

  if ($element['#value']['remainder'] < 0) {
    form_error($element['request'], t('@request cannot exceed @total', array(
      '@request' => $element['#header']['request'],
      '@total' => $element['#header']['total'],
    )));
  }
}

/**
 * Returns an empty budgetfield item.
 */
function _budgetfield_empty_item() {
  return array(
    'label' => '',
    'quantity' => '',
    'unitcost' => '',
    'request' => '',
  );
}

/**
 * Returns default header names.
 */
function _budgetfield_default_header() {
  // The order of these fields cannot be changed without potentially breaking
  // the table displays.
  //
  // @see _budgetfield_columns()
  return array(
    'label' => t('Description'),
    'quantity' => t('Quantity'),
    'unitcost' => t('Unit Cost'),
    'request' => t('Request'),
    'remainder' => t('Remainder'),
    'total' => t('Total'),
  );
}

/**
 * Return keys for the default header columns.
 */
function _budgetfield_columns() {
  return array_keys(_budgetfield_default_header());
}
